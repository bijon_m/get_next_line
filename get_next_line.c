/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <mbijon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 16:22:05 by mbijon            #+#    #+#             */
/*   Updated: 2018/02/28 11:42:02 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "get_next_line.h"
#include "libft/libft.h"

int				ft_count(char *str, int flag)
{
	int			i;

	i = 0;
	while (str != NULL && str[i])
	{
		if (flag == 1 && str[i] == '\n')
			return (i);
		i++;
	}
	return (i);
}

char			*ft_realloc(char *save, char *buf)
{
	int			i;
	int			j;
	char		*tmp;

	i = 0;
	j = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (ft_count(save, 0) +
		ft_count(buf, 0) + 1))) == NULL)
		return (NULL);
	while (save != NULL && save[i])
	{
		tmp[i] = save[i];
		i++;
	}
	while (buf != NULL && buf[j])
		tmp[i++] = buf[j++];
	tmp[i] = '\0';
	if (save != NULL)
		free(save);
	return (tmp);
}

static char		*ft_strndup(char *src, int size)
{
	int			i;
	char		*tmp;

	i = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (size + 1))) == NULL)
		return (NULL);
	while (src != NULL && src[i] && i < size)
	{
		tmp[i] = src[i];
		i++;
	}
	tmp[i] = '\0';
	return (tmp);
}

int				ret_get(char **save, char **line)
{
	int			ret_new_line;

	ret_new_line = ft_count(*save, 1);
	*line = ft_strndup(*save, ret_new_line);
	if (ft_count(&((*save)[ret_new_line + 1]), 0) > 0)
	{
		if ((*save = ft_realloc(NULL, &((*save)[ret_new_line + 1]))) == NULL)
			return (-1);
	}
	else
	{
		free(*save);
		*save = NULL;
	}
	return (1);
}

int				get_next_line(const int fd, char **line)
{
	static char	*save = NULL;
	int			ret;
	int			ret_new_line;
	char		buf[BUFF_SIZE + 1];

	if (fd == -1 || BUFF_SIZE < 0 || line == NULL)
		return (-1);
	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		if ((save = ft_realloc((char *)save, buf)) == NULL)
			return (-1);
		if (ft_count(save, 0) != (ret_new_line = ft_count(save, 1)))
			return (ret_get(&save, line));
	}
	if (ft_count(save, 0) > 0)
		return (ret_get(&save, line));
	if (ret < 0)
		return (-1);
	*line = NULL;
	return (0);
}
