/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:53:08 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 11:53:11 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned char	*tmp;
	unsigned char	*tmp1;
	size_t			i;

	i = 0;
	tmp = dest;
	tmp1 = (unsigned char *)src;
	if (n == 0)
		return (dest);
	while (i < n)
	{
		tmp[i] = tmp1[i];
		i++;
	}
	return ((void *)tmp);
}
