/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:38:06 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 12:38:08 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

static int	my_strlen(const char *str)
{
	int		i;

	i = 0;
	while (str != NULL && str[i])
		i++;
	return (i);
}

void		ft_putendl_fd(char const *s, int fd)
{
	write(fd, s, my_strlen(s));
	write(fd, "\n", 1);
}
