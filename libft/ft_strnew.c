/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:31:42 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 12:31:44 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char			*ft_strnew(size_t size)
{
	char		*tmp;
	size_t		i;

	i = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (size + 1))) == NULL)
		return (NULL);
	while (i <= size)
		tmp[i++] = '\0';
	return (tmp);
}
