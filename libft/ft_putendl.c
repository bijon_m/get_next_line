/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:36:54 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 12:36:55 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

static int	my_strlen(const char *str)
{
	int		i;

	i = 0;
	while (str != NULL && str[i])
		i++;
	return (i);
}

void		ft_putendl(char const *s)
{
	write(1, s, my_strlen(s));
	write(1, "\n", 1);
}
