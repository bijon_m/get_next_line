/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:47:37 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 12:21:17 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

static int	my_sign(const char *nptr)
{
	int		i;

	i = 0;
	while (nptr != NULL && nptr[i] && (nptr[i] == ' ' || nptr[i] == '\v' ||
			nptr[i] == '\t' || nptr[i] == '\n' || nptr[i] == '\f' ||
			nptr[i] == '\r'))
		i++;
	return (i);
}

static int	my_res(const char *nptr, int i, int sign)
{
	int		res;

	res = 0;
	while ((nptr != NULL && nptr[i]) && nptr[i] >= '0' && nptr[i] <= '9')
	{
		if ((res * sign) > 0 && sign == -1)
			return (0);
		else if ((res * sign) < 0 && sign == 1)
			return (-1);
		res = (res * 10) + (nptr[i] - 48);
		i++;
	}
	return (res * sign);
}

int			ft_atoi(const char *nptr)
{
	int		res;
	int		i;
	int		sign;

	i = 0;
	res = 0;
	sign = 1;
	i = my_sign(nptr);
	if (nptr != NULL && nptr[i] == '-')
		sign = -1;
	i = my_sign(nptr);
	if (nptr[i] && (nptr[i] == '+' || nptr[i] == '-'))
	{
		if (nptr[i + 1] == '-' || nptr[i + 1] == '+')
			return (0);
		i++;
	}
	return (my_res(nptr, i, sign));
}
