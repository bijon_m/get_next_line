/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:52:50 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 11:52:55 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	c1;
	char			*tmp;
	unsigned char	*tmp1;

	tmp = (char *)dest;
	tmp1 = (unsigned char *)src;
	c1 = (unsigned char)c;
	i = 0;
	while (i < n)
	{
		tmp[i] = tmp1[i];
		if (tmp1[i] == c1)
			return (&dest[i + 1]);
		i++;
	}
	return (NULL);
}
