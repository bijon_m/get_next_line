/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 16:59:28 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/15 16:59:33 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*tmp;
	t_list	*end;

	end = NULL;
	if ((tmp = (t_list *)malloc(sizeof(t_list))) == NULL)
		return (NULL);
	while (lst != NULL)
	{
		tmp = f(lst);
		tmp = tmp->next;
		lst = lst->next;
	}
	tmp = end;
	return (tmp);
}
