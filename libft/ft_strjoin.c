/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:34:40 by mbijon            #+#    #+#             */
/*   Updated: 2018/02/28 11:34:38 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strjoin(char const *s1, char const *s2)
{
	char	*tmp;
	int		i;
	int		j;

	if ((tmp = (char *)malloc(sizeof(char) * (ft_strlen(s1) +
		ft_strlen(s2) + 1))) == NULL)
		return (NULL);
	i = 0;
	while (s1 != NULL && s1[i])
	{
		tmp[i] = s1[i];
		i++;
	}
	j = 0;
	while (s2 != NULL && s2[j])
		tmp[i++] = s2[j++];
	tmp[i] = '\0';
	if (s1 != NULL)
		free((char *)s1);
	return (tmp);
}
