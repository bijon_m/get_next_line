/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:15:15 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/28 14:34:46 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char	*tmp1;
	unsigned char	*tmp2;
	size_t			i;

	i = 0;
	tmp2 = (unsigned char *)dest;
	tmp1 = (unsigned char *)src;
	if (src > dest)
	{
		while (i < n)
		{
			tmp2[i] = tmp1[i];
			i++;
		}
	}
	else
	{
		while ((int)n > 0)
		{
			tmp2[n - 1] = tmp1[n - 1];
			n--;
		}
	}
	return (dest);
}
