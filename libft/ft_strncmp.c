/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:50:50 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 11:50:52 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_strncmp(const char *s1, const char *s2, size_t n)
{
	int	i;

	i = 0;
	if (n == 0)
		return (0);
	while ((s1[i] && s2[i]) && ((unsigned char)s1[i] == (unsigned char)s2[i]
			&& i < (int)n))
		i++;
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}
