/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 13:04:31 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/27 18:09:50 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	pos_last_alpha(char const *s, int i)
{
	int		j;

	j = 0;
	while (s[i])
	{
		if (s[i] != ' ' && s[i] != '\t' && s[i] != '\n')
			j = i;
		i++;
	}
	return (j);
}

char		*ft_strtrim(char const *s)
{
	char	*tmp;
	int		last_char;
	int		ant;
	int		i;
	int		j;

	i = 0;
	j = 0;
	tmp = NULL;
	while (s[i] && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t'))
		i++;
	last_char = pos_last_alpha(s, i);
	if (last_char <= 0 && (s[last_char] == ' ' || s[last_char] == '\t' ||
		s[last_char] == '\n'))
		return (ft_strdup(""));
	if ((tmp = (char *)malloc(sizeof(char) * ((last_char - i) + 2))) == NULL)
		return (NULL);
	ant = i;
	while (j <= (last_char - ant))
		tmp[j++] = s[i++];
	tmp[j] = '\0';
	return (tmp);
}
