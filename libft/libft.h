/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:44:06 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/14 12:49:22 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>

/*
**								Function : LIST
*/

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

/*
**								Function : LIST
*/

/*
** Return a new link
*/
t_list				*ft_lstnew(void const *content, size_t content_size);

/*
** Add a link
*/
void				ft_lstadd(t_list **alst, t_list *new);

/*
** Release link
*/
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));

/*
** Browse the chained link and release each link
*/
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));

/*
** Traverses the linked list and applies the function to each link
*/
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));

/*
** Traverses the linked list and applies the function to each link and puts
** the result in a new string
*/
t_list				*ft_lstmap(t_list *list, t_list *(*f)(t_list *elem));

/*
**								Function : cut a string of c in double tab
*/

/*
** Return a double tab result of cut s to c
*/
char				**ft_strsplit(char const *s, char c);

/*
**								Function : DISPLAY
*/

/*
** write a character c with \n
*/
void				ft_putchar(char c);

/*
** write a string s
*/
void				ft_putstr(char const *s);

/*
** write a string s with \n
*/
void				ft_putendl(char const *s);

/*
** write a number n
*/
void				ft_putnbr(int n);

/*
** write in fd a character c
*/
void				ft_putchar_fd(char c, int fd);

/*
** write in fd a string s
*/
void				ft_putstr_fd(char const *s, int fd);

/*
** write in fd a string s with \n
*/
void				ft_putendl_fd(char const *s, int fd);

/*
** write in fd a number n
*/
void				ft_putnbr_fd(int n, int fd);

/*
**								Function : APPLIES A FUNCTION
**											TO EACH CHARACTER
*/

/*
** Applies the function to each character of string
*/
void				ft_striter(char *s, void (*f)(char *));

/*
** Applies the function to each character of string
*/
void				ft_striteri(char *s, void (*f)(unsigned int, char *));

/*
** Applies the function to each character of string and return
** a new string with result of function
*/
char				*ft_strmap(char const *s, char (*f)(char));

/*
** Applies the function to each character of string and return
** a new string with result of function
*/
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));

/*
**								Function : FREE POINTER
*/

/*
** Free a pointer
*/
void				ft_strdel(char **as);

/*
** Free a pointer
*/
void				ft_memdel(void **ap);

/*
**								Function : SIZE
*/

/*
** Return the size of s
*/
size_t				ft_strlen(const char *s);

/*
** Return the size of dst and src
*/
size_t				ft_strlcat(char *dst, const char *src, size_t size);

/*
**								Function : SET THE STRING
*/

/*
** Fill c characters in s
*/
void				*ft_memset(void *s, int c, size_t n);

/*
** Fill \0 characters in s at n
*/
void				ft_bzero(void *s, size_t n);

/*
** Fill \0 characters in s
*/
void				ft_strclr(char *s);

/*
**								Function : ALLOCATION
*/

/*
** Alloue une chaine de caractere
*/
char				*ft_strdup(const char *s);

/*
** Alloue une chaine de caractere and set 0
*/
void				*ft_memalloc(size_t size);

/*
** Alloue une chaine de caractere and set 0
*/
char				*ft_strnew(size_t size);

/*
**								Function : REPLACE
*/

/*
** Replace dest for src
*/
char				*ft_strcpy(char *dest, const char *src);

/*
** Replace dest for src in n characters
*/
char				*ft_strncpy(char *dest, const char *src, size_t n);

/*
** Replace dest for src in n characters
*/
void				*ft_memmove(void *dest, const void *src, size_t n);

/*
** Replace dest for src in n characters
*/
void				*ft_memcpy(void *dest, const void *src, size_t n);

/*
**								Function : CONCATENATION
*/

/*
** Return in dest + src
*/
char				*ft_strcat(char *dest, const char *src);

/*
** Return in dest + (src in characters)
*/
char				*ft_strncat(char *dest, const char *src, size_t n);

/*
** Return a new string with s1 plus s2
*/
char				*ft_strjoin(char const *s1, char const *s2);

/*
**								Function : RETURN STRING
*/

/*
** Return needle if in haystack
*/
char				*ft_strstr(const char *haystack, const char *needle);

/*
** Return needle if in haystack in len charcters
*/
char				*ft_strnstr(const char *haystack, const char *needle,
								size_t len);

/*
** Return n character dest if c equal src[n]
*/
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);

/*
** Return the first occurence equal c
*/
char				*ft_strchr(const char *s, int c);

/*
** Return the last occurence equal c
*/
char				*ft_strrchr(const char *s, int c);

/*
** Return a new string of start in s and size len
*/
char				*ft_strsub(char const *s, unsigned int start, size_t len);

/*
** Return a new string without space at start and end of string
*/
char				*ft_strtrim(char const *s);

/*
**								Function : COMPARISON STRING
*/

/*
** Return the first difference
*/
int					ft_strcmp(const char *s1, const char *s2);

/*
** Return the first difference in n characters
*/
int					ft_strncmp(const char *s1, const char *s2, size_t n);

/*
** Return the first difference in n characters
*/
int					ft_memcmp(const void *s1, const void *s2, size_t n);

/*
** Return the first occurence with c in n characters
*/
void				*ft_memchr(const void *s, int c, size_t n);

/*
** Return 0 si false return 1 if s1 egual s2 in characters
*/
int					ft_strnequ(char const *s1, char const *s2, size_t n);

/*
** Return 0 si false return 1 if s1 egual s2
*/
int					ft_strequ(char const *s1, char const *s2);

/*
**								Function : CONVERT
*/

/*
**	Converts a string to number
*/
int					ft_atoi(const char *nptr);

/*
** Convert a number to string
*/
char				*ft_itoa(int n);

/*
**								Function : CHARACTER
*/

/*
** Converts the letter to lower
*/
int					ft_tolower(int c);

/*
** Converts the letter to upper
*/
int					ft_toupper(int c);

/*
** Return no zero if printable
*/
int					ft_isprint(int c);

/*
** Return no zero if ascii
*/
int					ft_isascii(int c);

/*
** Return no zero if alpha and digit
*/
int					ft_isalnum(int c);

/*
**Return no zero if digit
*/
int					ft_isdigit(int c);

/*
** Return no zero if alpha
*/
int					ft_isalpha(int c);

#endif
