/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:50:20 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/28 13:01:49 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		result(char *dst, const char *src, size_t k, size_t size)
{
	int			j;

	j = 0;
	while (src[j])
	{
		if ((size - 1) > k)
		{
			dst[k++] = src[j];
		}
		j++;
	}
	dst[k] = '\0';
	return (k);
}

size_t			ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t		i;
	size_t		k;
	size_t		h;

	h = size;
	i = 0;
	k = 0;
	if (size == 0)
		return (0);
	while (dst[i] != '\0' && h-- > 0)
	{
		i++;
	}
	if (i == size)
		return (i + ft_strlen(src));
	k = i;
	k = result(dst, src, k, size);
	return (i + k + ft_strlen(src) - k);
}
