/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:34:28 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 12:34:30 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*tmp;
	size_t	i;

	i = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (len + 1))) == NULL)
		return (NULL);
	while (s != NULL && s[start] && i < len)
		tmp[i++] = s[start++];
	tmp[i] = '\0';
	return (tmp);
}
