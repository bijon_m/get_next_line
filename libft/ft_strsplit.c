/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:35:19 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 12:35:22 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int		my_strlen(char const *s, char c)
{
	size_t		i;
	int			j;

	j = 0;
	i = 0;
	while (s != NULL && s[i])
	{
		if ((i == 0 || i == ft_strlen(s)) && s[i] == c)
			i++;
		while (s[i] == c && s[i + 1] == c)
			i++;
		i++;
		j++;
	}
	return (j);
}

static char		*ft_strndup(char const *s, int pos, int size)
{
	int			i;
	char		*tmp;

	i = 0;
	if (size == 0)
		return (NULL);
	if ((tmp = (char *)malloc(sizeof(char) * (size + 1))) == NULL)
		return (NULL);
	while (s[pos] && i < size)
		tmp[i++] = s[pos++];
	tmp[i] = '\0';
	return (tmp);
}

char			**ft_strsplit(char const *s, char c)
{
	char		**tmp;
	char		*str;
	int			i;
	int			j;
	int			ant;

	i = 0;
	j = 0;
	tmp = NULL;
	if ((tmp = (char **)malloc(sizeof(char *) * (my_strlen(s, c) + 1)))
		== NULL)
		return (NULL);
	while (s != NULL && s[i])
	{
		ant = i;
		while (s[i] && s[i] != c)
			i++;
		if ((str = ft_strndup(s, ant, i - ant)) != NULL)
			tmp[j++] = str;
		if (s[i] != '\0')
			i++;
	}
	tmp[j] = NULL;
	return (tmp);
}
