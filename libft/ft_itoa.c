/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:35:43 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 12:35:44 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static char		*my_strdup(int n, int i)
{
	char		*tmp;
	int			j;
	long		div;

	j = 0;
	div = 1;
	while (n > div)
	{
		div *= 10;
		j++;
	}
	if ((tmp = (char *)malloc(sizeof(char) * (j + i + 1))) == NULL)
		return (NULL);
	if (i == 1)
		tmp[0] = '-';
	if (n < div)
		div /= 10;
	while (div >= 1)
	{
		tmp[i] = (n / div) % 10 + 48;
		i++;
		div /= 10;
	}
	tmp[i] = '\0';
	return (tmp);
}

char			*ft_itoa(int n)
{
	char		*tmp;
	int			i;

	i = 0;
	tmp = NULL;
	if (n == -2147483648)
		return ((ft_strdup("-2147483648")));
	else if (n == 0)
		return (ft_strdup("0"));
	else
	{
		i = 0;
		if (n < 0)
		{
			i = 1;
			n *= -1;
		}
		tmp = my_strdup(n, i);
	}
	return (tmp);
}
